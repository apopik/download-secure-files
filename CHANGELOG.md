## v0.1.5

Fixes a bug causing downloads to fail when a path exists in the filename https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/5
## v0.1.4

Updated file download limit from 20 to 100 files
## v0.1.3

Documentation and testing updates

## v0.1.2

Adding a downstream CI pipeline to test various distribution builds

## v0.1.1

Adding installer script

## v0.1.0

Initial Release
